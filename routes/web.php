<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->version();
});

//The namespace can also be overwritten in bootstrap/app.php
$router->group([
	'namespace'=>'\App\Api'],function($router){

		$router->post('login',['as' => 'login','uses' => 'Login\LoginController@login']);
		$router->post('register',['as' => 'login.register','uses' => 'Login\LoginController@register']);
		$router->get('register/verifyEmail/{token}',['as' => 'register.verifyEmail','uses' => 'Login\LoginController@verifyEmail']);
		
		$router->get('refresh',['as' => 'login.refresh', 'uses' => 'Login\LoginController@refresh','middleware' => ['jwt.auth','refresh.token']]);
		
		$router->group(['middleware' => ['jwt.auth','access.token']], function($router){
			//protected routes go here
			$router->group(['prefix' => 'addresses'],function($router){
				$router->get('',['uses' => 'Addresses\AddressController@index']);
				$router->post('',['uses' => 'Addresses\AddressController@store']);
				$router->get('{id}',['uses' => 'Addresses\AddressController@show']);
				$router->patch('{id}',['uses' => 'Addresses\AddressController@update']);
				$router->delete('{id}',['uses' => 'Addresses\AddressController@destroy']);
				$router->get('setDefault/{id}',['uses' => 'Addresses\AddressController@setDefault']);
				$router->get('activate/{id}',['uses' => 'Addresses\AddressController@activate']);
				$router->get('deactivate/{id}',['uses' => 'Addresses\AddressController@deactivate']);
			});

			$router->group(['prefix' => 'categories'],function($router){
				$router->get('',['uses' => 'Categories\CategoryController@index']);
				$router->post('',['uses' => 'Categories\CategoryController@store']);
				$router->get('{id}',['uses' => 'Categories\CategoryController@show']);
				$router->patch('{id}',['uses' => 'Categories\CategoryController@update']);
				$router->delete('{id}',['uses' => 'Categories\CategoryController@destroy']);
				// $router->get('activate/{id}',['uses' => 'Categories\CategoryController@activate']);
				// $router->get('deactivate/{id}',['uses' => 'Categories\CategoryController@deactivate']);
			});

			$router->group(['prefix' => 'activities'],function($router){
				$router->get('',['uses' => 'Activities\ActivityController@index']);
				$router->post('',['uses' => 'Activities\ActivityController@store']);
				$router->get('{id}',['uses' => 'Activities\ActivityController@show']);
				$router->patch('{id}',['uses' => 'Activities\ActivityController@update']);
				$router->delete('{id}',['uses' => 'Activities\ActivityController@destroy']);
				// $router->get('activate/{id}',['uses' => 'Categories\CategoryController@activate']);
				// $router->get('deactivate/{id}',['uses' => 'Categories\CategoryController@deactivate']);
			});
			
			$router->group(['prefix' => 'users'],function($router){
				$router->get('',['uses' => 'User\UserController@index']);
				$router->post('searchTrainers/{distance}',['uses' => 'User\UserController@searchTrainers']);
			});

		});
		

		
		// $router->group(['middleware' => 'api.auth'],function($router){
			
		// 	$router->resource('activities','Activities\ActivityController',['only' => ['index','show']]);
		// 	$router->resource('categories','Categories\CategoryController',['only' => ['index','show']]);
			
		// 	$router->get('user/activities','ActivityUser\ActivityUserController@index');
		// 	$router->post('user/activities/{activity_id}/attach','ActivityUser\ActivityUserController@attach');
		// 	$router->post('user/activities/{activity_id}/detach','ActivityUser\ActivityUserController@detach');
		// });
});