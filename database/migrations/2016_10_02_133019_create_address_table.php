<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->string('number');
            $table->string('street');
            $table->string('street2')->nullable();
            $table->string('postcode');
            $table->string('county');
            $table->string('country');                           
            $table->point('location');
            $table->spatialIndex('location');
            // Add a spatial index on the location field
            $table->boolean('active')->default(1);
            $table->boolean('isDefault')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
