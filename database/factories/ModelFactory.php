<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Api\User\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => '1234',
        // 'remember_token' => str_random(10),
        'mobile'=>$faker->randomNumber(9),
        'verified'=>true
    ];
});

$factory->define(App\Api\Devices\Device::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Device 1',
        'user_id' => 1,
    ];
});
