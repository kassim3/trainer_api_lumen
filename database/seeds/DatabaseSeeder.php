<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $users = factory(App\Api\User\User::class, 1)->create();
        $users = factory(App\Api\Devices\Device::class, 1)->create();
        
        //  DB::table('categories')->insert(['name' => 'cardio']);
        
        //  DB::table('activities')->insert(['name' => 'running','category_id' => 1]);
        //  DB::table('activities')->insert(['name' => 'weights','category_id' => 1]);
        //  DB::table('activities')->insert(['name' => 'jumping','category_id' => 1]);
    }
}
