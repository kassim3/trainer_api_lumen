<?php

namespace App\Api\Activities;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

use App\Api\Controller;
use App\Api\Categories\Category;
use Validator;

class ActivityController extends Controller
{
     protected $activity;

    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'activities' => $this->activity->all()
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $fields = $request->all();
        $rules = ['name'=> 'required|max:255','category_id' => 'required|numeric'];

        $validator = \Validator::make($fields,$rules,['category_id.required' => 'Category must be selected']);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => ['validation' => $validator->errors()]
            ],400);

        $category = Category::find($fields['category_id']);

        if(!$category)
            return response()->json([
                'status' => 'error',
                'message' => 'Category is invalid'
            ],400);



        $activity = new Activity([
            'name' => $fields['name']
        ]);

        $category->activities()->save($activity);

        return response()->json([
            'status' => 'success',
            'data' => [
                'activity' => $activity
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $activity = $this->activity->find($id);

        if(!$activity)
            return response()->json([
                'status' => 'error',
                'message' => 'Activity not found'
            ],404);


        return response()->json([
            'status' => 'success',
            'data' => [
                'activity' => $activity
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            $activity = Activity::find($id);
            
            if(!$activity)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Address could not be found'
                ],404);

            //Finish off logic to update
            $this->validate($request, [
                'name' => 'string',
                'category_id' => 'numeric'
            ]);

            $fields = $request->all();

            if(!empty($fields['name']))
                $activity->name = $fields['name'];

            if(!empty($fields['category_id']))
            {
                $category = Category::find($fields['category_id']);

                if(!$category)
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Category is invalid'
                    ],400);

                $activity->category()->associate($category);
            }

            $activity->save();

            return response()->json([
                'status' => 'success',
                'data' => [
                    'activity' => $activity
                ]
            ]);
        }catch(\Illuminate\Validation\ValidationException $e){

                //change this to return pure json response
               return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $e->getResponse()->original]
                ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $activity = $this->activity->find($id);
        
        if(!$activity)
            return response()->json([
                'status' => 'error',
                'message' => 'Address could not be found'
            ],404);

        $activity->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
