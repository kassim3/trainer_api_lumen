<?php

namespace App\Api\Activities;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

	protected $fillable = [
		'name',
	];

    /**
     * Get activities associated with the category
     */
    public function category()
    {
    	return $this->belongsTo("App\Api\Categories\Category");
    }

    /**
     * Get users associated with the activity
     *
     * @return void
     */
    public function users()
    {
        return $this->belongsToMany('App\Api\Users\User');
    }

}
