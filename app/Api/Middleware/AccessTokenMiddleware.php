<?php

namespace App\Api\Middleware;

use Closure;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\PayloadException;

class AccessTokenMiddleware
{

    private $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        //After updating jwtAuth custom claims get added to addClaims property
        $tokenType = $this->jwt->getClaim('addClaims')['type'];

        if($tokenType <> 'access')
            throw new PayloadException('The token provided is not an access token');

        return $next($request);
    }
}
