<?php

namespace App\Api\Middleware;

use Closure;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\PayloadException;

class RefreshTokenMiddleware
{

    private $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tokenType = $this->jwt->getClaim('addClaims')['type'];
        
        if($tokenType <> 'refresh')
            throw new PayloadException('The token provided is not a refresh token');

        return $next($request);
    }
}
