<?php

namespace App\Api\Devices;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['name','refresh_token'];

    /**
     * Get the user associated with the device
     */
    public function user()
    {
    	return $this->belongsTo('App\Api\User\User');
    }
}
