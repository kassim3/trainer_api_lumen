<?php

namespace App\Api\User;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','surname','verification_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


  /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the devices for the user
     */
    public function devices()
    {
        return $this->hasMany('App\Api\Devices\Device');
    }

    /**
     * Get the addresses for the user
     */
    public function addresses()
    {
        return $this->hasMany('App\Api\Addresses\Address');
    }

    /**
     * Automatically hash the password when being set 
     * @param string $value The password value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);;
    }

    /**
     * Get the trainer for the user
     */
    public function trainer()
    {
        return $this->belongsTo('App\Api\Trainers\Trainer');
    }

    /**
     * The activities that favourited by the user
     */
    public function activities()
    {
        return $this->belongsToMany('App\Api\Activities\Activity');
    }
}
