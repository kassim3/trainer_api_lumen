<?php

namespace App\Api\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use App\Api\Addresses\Address;

use App\Api\Controller;
use Validator;


class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'user' =>  $request->user()
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

    }

    public function searchTrainers(Request $request,$distance)
    {
        $address = $request->user()->addresses()->where('isDefault',true)->first();
        $allAddress = Address::distanceSphereExcludingSelf('location',$address->location,$distance * 1609.344)->get();

        return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $allAddress
            ]
        ]);
    }
}
