<?php

namespace App\Api;

use Illuminate\Support\ServiceProvider;

class TrainerApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->register(\Tymon\JWTAuth\Providers\LumenServiceProvider::class);
        $this->app->register(\Illuminate\Mail\MailServiceProvider::class);
        $this->app->register(\Grimzy\LaravelMysqlSpatial\SpatialServiceProvider::class);
        $this->app->configure('mail');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        if ($this->app->environment() == 'local') {
            // $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
            // $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        // $this->app->bind('App\Api\Login\ILoginService','App\Api\Login\LoginService');
        // $this->app->bind('App\Api\V1\Register\IRegisterService','App\Api\V1\Register\RegisterService');
        // $this->app->bind('App\Api\V1\Addresses\IAddressRepository','App\Api\V1\Addresses\AddressRepository');
        // $this->app->bind('App\Api\V1\Trainers\ITrainerRepository','App\Api\V1\Trainers\TrainerRepository');
    }
}
