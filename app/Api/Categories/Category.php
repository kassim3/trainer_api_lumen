<?php

namespace App\Api\Categories;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $fillable = [
		'name',
	];

    /**
     * Get activities associated with the category
     */
    public function activities()
    {
    	return $this->hasMany("App\Api\Activities\Activity");
    }
	
}
