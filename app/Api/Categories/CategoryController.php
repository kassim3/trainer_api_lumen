<?php

namespace App\Api\Categories;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

use App\Api\Controller;
use Validator;


class CategoryController extends Controller
{
     protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'categories' => $this->category->all()
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        try{
            $fields = $request->all();
            $this->validate($request, [
                'name'    => 'required|max:255'
            ]);

            $category = $this->category->create([
                'name' => $fields['name']
            ]);

            return response()->json([
                'status' => 'success',
                'data' => [
                    'category' => $category
                ]
            ]);

        }catch(ValidationException $e){
            return response()->json([
                'status' => 'error',
                'message' => ['validation' => $e->response->original]
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $category = $this->category->find($id);

        if(!$category)
            return response()->json([
                'status' => 'error',
                'message' => 'Category not found'
            ],404);


        return response()->json([
            'status' => 'success',
            'data' => [
                'category' => $category
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            $category = $this->category->find($id);
            
            if(!$category)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Address could not be found'
                ],404);

            //Finish off logic to update
            $this->validate($request, [
                'name' => 'required|string'
            ]);

            $fields = $request->all();
            $category->name = $fields['name'];
            $category->save();

            return response()->json([
                'status' => 'success',
                'data' => [
                    'category' => $category
                ]
            ]);
        }catch(\Illuminate\Validation\ValidationException $e){

                //change this to return pure json response
               return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $e->getResponse()->original]
                ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $category = $this->category->find($id);
        
        if(!$category)
            return response()->json([
                'status' => 'error',
                'message' => 'Address could not be found'
            ],404);

        $category->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
