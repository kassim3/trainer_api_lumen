<?php

namespace App\Api\Login\Exceptions;

use Exception;

class RefreshTokenInvalidException extends Exception {}