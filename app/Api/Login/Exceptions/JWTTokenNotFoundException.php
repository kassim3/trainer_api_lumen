<?php

namespace App\Api\Login\Exceptions;

use Exception;

class JWTTokenNotFoundException extends Exception {}