<?php

namespace App\Api\Login\Exceptions;

use Exception;

class TokenNotGeneratedException extends Exception {}