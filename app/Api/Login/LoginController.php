<?php

namespace App\Api\Login;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Tymon\JWTAuth\JWTAuth;

use DB;
use Validator;
use App\Api\Controller;
use App\Api\Login\LoginService;
use App\Api\User\User;
use App\Api\Trainers\Trainer;


// use App\Http\Requests\LoginUserRequest;

class LoginController extends Controller
{
    /**
     * Request instance
     * @var Illuminate\Http\Request
     */
    protected $request;

    /**
     * Login Service instance
     * @var App\Api\V1\Login\LoginService
     */
    protected $loginService;

    /**
	 * The JWTAuth instance
	 * @var JWTAuth
	 */
	protected $jwt;

    public function __construct(Request $request, LoginService $loginService, JWTAuth $jwt)
    {
        $this->request = $request;
        $this->loginService = $loginService;
        $this->jwt = $jwt;
    }

    /**
     * Create a new user refresh token and JWT.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function login(Request $request)
    {
        DB::beginTransaction();

        try{
            $validator = Validator::make($request->all(), [
                'email'    => 'required|email|max:255',
                'password' => 'required',
                'device_name' => 'required|max:255'
            ]);

            if(count($validator->errors())> 0)
                return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $validator->errors()]
                    ],400);

            $device_name = $this->request->input('device_name');
            $device = $this->loginService->saveDeviceName($device_name);
            
            $credentials = $request->only('email', 'password');
            $token = $this->loginService->login($credentials);
            
            if(!$this->jwt->user()->verified)
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Account not verified'
                    ],400);
            
            $this->loginService->attachDeviceToUser($device,$token);

            $refresh_token = $this->loginService->createRefreshToken($device);
            
            DB::commit();

            return response()->json([

                'status' => 'success',
                'data' => [
                    'token' => $token,
                    'refresh_token' => $refresh_token,
                    'totalAddresses' => $this->jwt->user()->addresses()->count()
                ]
            ]);
            
        }catch(Exceptions\TokenNotGeneratedException $e){

            DB::rollBack();

            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage()
            ],401);
        }
    }

    /**
     * Remove the refresh the refresh token of the user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout($id)
    {
        //
    }

    /**
     * Genereate a new JWT when the JWT expires based on the refresh token
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function refresh(Request $request)
    {

        try{

            $token = $this->loginService->regenerateToken($request);

            if(empty($token))
                throw new Exceptions\TokenNotGeneratedException('token not generated');

                return response()->json([

                    'status' => 'success',
                    'data' => [
                        'token' => $token
                    ]
                ]);
        }catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],401);
        }

    }

    public function register(Request $request)
	{
		DB::beginTransaction();

		try{
            $fields = $request->all();
			$validator = Validator::make($request->all(),[
                'email'=>'required|email|unique:users',
                'password'=>'required',
                'name'=>'required',
                'surname'=>'required',
                'mobile'=>'required|unique:users'
			]);

            if(count($validator->errors())> 0)
                return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $validator->errors()]
                ],400);


            $user = new User([
            	'email' => $fields['email'],
				'password' => $fields['password'],
				'name' => $fields['name'],
				'surname' => $fields['surname'],
				'mobile' => $fields['mobile'],
				'verification_token' => str_random(60)
            ]);


			if(!empty($fields['isTrainer']))
			{
                $trainer = new Trainer([
                    'price' => $fields['price'],
                    'days' => $fields['days']
                ]);
                $trainer->save();
                $user->trainer()->associate($trainer);
			}

            $user->save();

			$this->loginService->sendEmailVerification($user);
			DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => [
                    'user' => $user
                ]
            ]);
        }catch(\Exception $e){

			DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],400);
		}
	}

	public function verifyEmail($token)
	{
		$user = $this->loginService->verifyEmail($token);
        return response()->json(['status' => 'success']);
		//redirect user to a web page to say email is verified and they can now login into the app
	}
}
