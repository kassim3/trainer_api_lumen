<?php

namespace App\Api\Login;


use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Parser\Parser;

use App\Api\User\User;
use App\Api\Devices\Device;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

// use App\Api\V1\Login\ILoginService;

class LoginService
{
	/**
	 * The JWTAuth instance
	 * @var JWTAuth
	 */
	protected $jwt;

	protected $jwtFactory;

    /**
     * The User model instance
     * @var App\Api\User\User
     */
    protected $user;

    /**
     * The Device instance
     * @var App\Api\V1\Devices\Device
     */
    protected $device;

    
    protected $mail;


    public function __construct(User $user, JWTAuth $jwt, Factory $jwtFactory)
    {
        $this->jwt = $jwt;
        $this->user = $user;        
        $this->jwtFactory = $jwtFactory;        
    }

    public function saveDeviceName($device_name)
    {
        $device = new Device(['name'=>$device_name]);
        return $device;
    }

    public function login($credentials)
    {
        $this->jwtFactory->addClaims(['type'=>'access']);
        if (!$token = $this->jwt->attempt($credentials)) 
            throw new Exceptions\TokenNotGeneratedException('Login failed');

        $this->jwtFactory->emptyClaims();
        return $token;
    }

    public function attachDeviceToUser($device,$token)
    {
        $this->jwt->setToken($token);
        $this->jwt->user()->devices()->save($device);
    }
    
    public function createRefreshToken($device)
    {
        $this->jwtFactory->addClaims(['type'=>'refresh']);
        
        //Extract this to config
        $this->jwtFactory->setTTL(43800);

        if(!$refresh_token = $this->jwt->fromUser($this->jwt->user()))
            throw new Exceptions\RefreshTokenInvalidException('Refresh token not set');

        $this->jwt->setToken($refresh_token);

        $device->refresh_token = $this->jwt->getClaim('jti');
        $device->save();

        $this->jwtFactory->emptyClaims();

        return $refresh_token;
    }

    public function sendEmailVerification($user)
    {
    	Mail::send('emails.emailVerification',['user'=>$user],function($m) use ($user){
    		$m->to($user->email,$user->name)->subject('Trainer App - Registration');
    	});
    }

    public function verifyEmail($token)
    {
    	$user = $this->user->where('verification_token','=',$token)->first();
        if(!$user)
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User not found');

		$user->verified = TRUE;
		$user->verification_token = NULL;
		$user->save();
        return $user;
    }

  
    public function regenerateToken()
    {
        $user = $this->jwt->user();

        $device = $user->devices()->where('refresh_token',$this->jwt->getClaim('jti'))->first();
        
        if(!$device)
            throw new Exceptions\RefreshTokenInvalidException('Wrong refresh token');

        $jwtFactory = $this->jwt->factory();
        $jwtFactory->emptyClaims();
        $jwtFactory->addClaims(['type'=>'access']);
        $token = $this->jwt->fromUser($user);
        return $token;
    }
}
