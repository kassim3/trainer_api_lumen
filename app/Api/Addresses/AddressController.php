<?php

namespace App\Api\Addresses;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Validator;
use App\Api\Controller;
use Tymon\JWTAuth\JWTAuth;
use Grimzy\LaravelMysqlSpatial\Types\Point;


class AddressController extends Controller
{
    /**
     * Address repo instance
     * @var Addresses\Address
     */
    protected $address;

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'addresses' => $request->user()->addresses()->get()
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        try{
            $fields = $request->all();
            $validator = Validator::make($request->all(), [
                'description'    => 'required|max:255',
                'number' => 'required',
                'street' => 'required|max:255',
                'postcode' => 'required|max:255',
                'county' => 'required|max:255',
                'country' => 'required|max:255',
                'latitude' => ['required'],
                'longitude' => ['required']
            ]);

            if(count($validator->errors())> 0)
                return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $validator->errors()]
            ],400);

            $address = $request->user()->addresses()->create([
                'description' => $fields['description'],
                'number' => $fields['number'],
                'street' => $fields['street'],
                'postcode' => $fields['postcode'],
                'county' => $fields['county'],
                'country' => $fields['country'],
                'location' => new Point($fields['latitude'], $fields['longitude'])                
            ]);

            return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $address
                ]
            ]);

        }catch(Exceptions\AddressValidationFailed $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'address' => $request->user()->addresses()->where('id',$id)->get()
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try{
            $address = $request->user()->addresses()->where('id',$id)->first();
            
            if(!$address)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Address could not be found'
                ],404);

            //Finish off logic to update
            $this->validate($request, [
                'description' => 'required|string'
            ]);

            $fields = $request->all();
            $address->description = $fields['description'];
            $address->save();

            return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $request->user()->addresses()->where('id',$id)->get()
                ]
            ]);
        }catch(\Illuminate\Validation\ValidationException $e){

            // dd();
                //change this to return pure json response
               return response()->json([
                    'status' => 'fail',
                    'message' => ['validation' => $e->getResponse()->original]
                ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $address = $request->user()->addresses()->where('id',$id)->first();

        if(!$address)
            return response()->json([
                'status' => 'error',
                'message' => 'Address not found'
            ],404);

        $address->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function setDefault(Request $request,$id)
    {
        $address = $request->user()->addresses()->where('id',$id)->first();
        
        if(!$address)
            return response()->json([
                'status' => 'error',
                'message' => 'Address not found'
            ],404);
        
        if($address->isDefault)
            return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $address
                ]
            ]);
        
        if(!$address->active)
            return response()->json([
                'status' => 'fail',
                'message' => 'Can\'t set an inactive address as default'
            ],400);

        $defaultAddresses = $request->user()->addresses()->where('isDefault',1)->get();

        foreach ($defaultAddresses as $defaultAddress) 
        {
            $defaultAddress->isDefault = false;
            $defaultAddress->save();
        }

        $address->isDefault = true;
        $address->save();

        return response()->json([
            'status' => 'success',
            'data' => [
                'address' => $address
            ]
        ]);
        
    }

    public function activate(Request $request,$id)
    {
        $address = $request->user()->addresses()->where('id',$id)->first();        

        if(!$address)
            return response()->json([
                'status' => 'error',
                'message' => 'Address not found'
            ],404);

        if(!$address->active)
        {
            $address->active = true;
            $address->save();
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'address' => $address
            ]
        ]);
    }

    public function deactivate(Request $request,$id)
    {
        $address = $request->user()->addresses()->where('id',$id)->first();        

        if(!$address)
            return response()->json([
                'status' => 'error',
                'message' => 'Address not found'
            ],404);

        if($address->active)
        {
            $address->active = false;
            $address->save();
        }

        if(!$address->active)
            return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $address
                ]
            ]);

    }
        
    public function searchTrainers(Request $request,$distance)
    {
        $address = $request->user()->addresses()->first();
        $allAddress = Address::distanceSphereExcludingSelf('location',$address->location,$distance * 1609.344)->get();

        return response()->json([
                'status' => 'success',
                'data' => [
                    'address' => $allAddress
                ]
            ]);
    }
        
}
