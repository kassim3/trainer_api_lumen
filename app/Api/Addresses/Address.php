<?php

namespace App\Api\Addresses;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Address extends Model
{
	use SpatialTrait;

	protected $fillable = [
		'description',
		'number',
		'street',
		'street2',
		'postcode',
		'county',
		'country',
		'latitude',
		'longitude',
		'location'
	];

	protected $spatialFields = [
        'location',
	];
	
    /**
     * Get the user associated with the device
     */
    public function user()
    {
    	return $this->belongsTo('App\Api\User\User');
    }
	
}
