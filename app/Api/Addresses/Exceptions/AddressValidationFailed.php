<?php

namespace App\Api\Addresses\Exceptions;

use Exception;

class AddressValidationFailed extends Exception {}